# Configure the Azure Provider
provider "azurerm" {
  version = "=2.0.0"
  features {}
}

# Create a resource group
resource "azurerm_resource_group" "tuffy-test" {
  name     = "tuffy-test"
  location = "West Europe"
}

terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "Ahead"

    workspaces {
      name = "test"
    }
  }
}
